#!/usr/bin/env bash

echoerr() { echo "$@" 1>&2; }

if [ "$#" -lt 3 ]; then
    echoerr "Usage: $0 firefox-version gitlab-audit-issue-number reviewers... > output.md"
    exit 1
fi

# Check pre-conditions
check_exists() {
    local cmd=$1
    if ! which ${cmd} > /dev/null ; then
        echoerr "missing ${cmd} dependency"
        exit 1
    fi
}

check_exists wget
check_exists jq
check_exists sed
check_exists perl

# assign arguments to named variables
firefox_version=$1
audit_issue=$2
reviewers="${@:3}"

# check valid esr version
if ! [[ "${firefox_version}" =~ ^[1-9][0-9]{2}$ ]]; then
    echoerr "invalid Firefox version (probably)"
    exit 1
fi

# check valid issue number
if ! [[ "${audit_issue}" =~ ^[1-9][0-9]{4}$ ]]; then
    echoerr "invalid gitlab audit issue number (probably)"
    exit 1
fi

# download bug list
json=/tmp/${firefox_version}.json
bugzilla_query="https://bugzilla.mozilla.org/buglist.cgi?j_top=OR&f1=target_milestone&o3=equals&v3=Firefox%20${firefox_version}o1=equals&resolution=FIXED&o2=anyexact&query_format=advanced&f3=target_milestone&f2=cf_status_firefox${firefox_version}&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&v1=mozilla128&v2=fixed%2Cverified&limit=0"
# you can get this from the 'REST' link at the bottom of the prevoius bugzilla query ^^;
bugzilla_json_query="https://bugzilla.mozilla.org/rest/bug?include_fields=id,summary,status&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&f1=target_milestone&f2=cf_status_firefox${firefox_version}&f3=target_milestone&j_top=OR&limit=0&o1=equals&o2=anyexact&o3=equals&resolution=FIXED&v1=mozilla128&v2=fixed%2Cverified&v3=Firefox%20${firefox_version}"

wget "${bugzilla_json_query}" -O ${json}

echo "### [Bugzilla Query](${bugzilla_query})"
echo ""

issue_count=$(jq '.bugs | length' ${json})
counter=0
jq '.bugs | sort_by(.id)[] | "\(.id)|\(.summary)"' ${json} | while IFS='|' read -r id summary; do

    # indexing
    counter=$((counter + 1))

    from=$counter
    through=$((counter + 499))
    if ((to > issue_count)); then
        to=$issue_count
    fi

    # break up into sections or else gitlab falls over
    if ((counter % 500 == 1)); then
        echo "<details>"
        echo "  <summary>Resolved Firefox ${firefox_version} Bugzilla Issues ${from} through ${through}</summary>"
        echo ""
    fi

    # bugzilla info
    id="${id:1}"
    summary="${summary:0:-1}"
    [[ ${#summary} -gt 90 ]] && summary_short="${summary:0:87}..." || summary_short="${summary}"

    # we need to escape printed strings for markdown
    md_escape() {
        local input="$1"
        # jesus I'm sorry
        echo "${input}" | sed 's/[][\\`*_{}<>()#+-\.~]/\\&/g'
    }

    md_summary=$(md_escape "${summary}")
    md_summary_short=$(md_escape "$summary_short")

    # we need to urlencode the strings used in the new issue link
    url_encode() {
        local input="$1"
        echo "${input}" | perl -MURI::Escape -wlne 'print uri_escape $_'
    }

    # parent issue
    bugzilla_url="https://bugzilla.mozilla.org/show_bug.cgi?id=${id}"
    # review issue title
    new_issue_title=$(url_encode "Review Mozilla ${id}: ${summary_short}")
    # review issue description
    new_issue_description=$(url_encode "### Bugzilla: ${bugzilla_url}")%0A$(url_encode "/label ~\"14.0 stable\" ~FF128-esr ~Next")%0A$(url_encode "/relate tpo/applications/tor-browser-spec#${audit_issue}")%0A%0A$(url_encode "<!-- briefly describe why this issue needs further review -->")%0A
    # url which create's new issue with title and description pre-populated
    new_issue_url="../../../../tor-browser/-/issues/new?issue[title]=${new_issue_title}&issue[description]=${new_issue_description}"

    # em-space
    em=" "
    counter_string=$(printf "%04i" ${counter})

    echo "- **${counter_string}**${em}<kbd>[Create Issue](${new_issue_url})</kbd>${em}[**${id}**: ${md_summary}](${bugzilla_url})"


    if ((counter % 500 == 0 )) || (( counter == issue_count )); then
        # checklist of engineers that have triaged this block
        echo "</details>"
        echo
        echo "**Triaged by:**"
        for reviewer in $reviewers; do
            echo "- [ ] **${reviewer}**"
        done
        echo
    elif ((counter % 25 == 0 )); then
        # add a hrule every 25 to break things up visually
        echo "---"
    fi
done
