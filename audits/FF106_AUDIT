# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `2dd649f09f70ec5b9304d62daeb427a86bbc5a36` ( `FIREFOX_105_0_3_RELEASE` )
- End:   `ac898d40ded7de23ef22a6f336f2ab1f0bca0d3f`  ( `FIREFOX_106_0_5_RELEASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `be8254df118b2fc2aae726e1d13ca4c982bec920`  ( `v94.3.1` )
- End:   `f1276e45b7c284bc4435896b1d5d09b35f3b295b`  ( `v95.0.1` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x ] rust

Nothing of interest (using `code_audit.sh`)

## Android Components: https://github.com/mozilla-mobile/android-components.git

- Start: `5bf7550ec9d5686fae721ee045132866f88afca6`
- End:   `5f06485fc33a26206ba4ef7996ba1ae83134e353`  ( `v106.0.5` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Fenix: https://github.com/mozilla-mobile/fenix.git

- Start: `74e6ce8103fe3d4525c0a91d8e0c0403d1fee44f` ( `v106.0b1` )
- End:   `71ca6c1f7a7a2f2045b82ed16be329acadc15084`  ( `v106.1.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Ticket Review ##

Bugzilla Query: `https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=106%20Branch&order=priority%2Cbug_severity&limit=0`

Nothing of interest (manual inspection)

#### Problematic Issues

- **Add the possibility to check that the clipboard contains some pdfjs stuff** https://bugzilla.mozilla.org/show_bug.cgi?id=1788668
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41950
  - **RESOLUTION** nothing to do here, verified no linkability concerns via testing
- **Enable seperatePrivateDefault by default** https://bugzilla.mozilla.org/show_bug.cgi?id=1790681
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41951
  - **RESOLUTION** nothing to do here, this is a tweak to a feature we have completley disabled

## Export
- [x] Export Report and save to `tor-browser-spec/audits`